return {
  {
    "echasnovski/mini.pairs",
    event = "InsertEnter",
    branch = "stable",
    opts = true,
  },

  {
    "nvim-tree/nvim-tree.lua",
    keys = {
      {
        "<leader>e",
        "<cmd>NvimTreeToggle<CR>",
        desc = "nvimtree toggle window",
      },
    },
    opts = {
      disable_netrw = true,
      hijack_cursor = true,
      sync_root_with_cwd = true,
      update_focused_file = {
        enable = true,
        update_root = false,
      },

      ui = {
        confirm = {
          trash = false,
        },
      },

      view = {
        number = true,
        relativenumber = true,
        width = 30,
        preserve_window_proportions = true,
        float = {
          enable = true,
        },
      },

      renderer = {
        highlight_modified = "all",
        highlight_opened_files = "all",

        root_folder_label = false,
        highlight_git = true,
        indent_markers = { enable = true },
        icons = {
          show = {
            git = true,
          },
          glyphs = {
            default = "󰈚",
            folder = {
              default = "",
              empty = "",
              empty_open = "",
              open = "",
              symlink = "",
            },
            git = { unmerged = "" },
          },
        },
      },

      filters = {
        dotfiles = false,
        git_ignored = false,
        custom = {
          ".git/",
          "__pycache__/",
          "node_modules/",
          ".project",
          ".classpath",
          ".DS_Store",
        },
      },

      filesystem_watchers = {
        ignore_dirs = {
          "__pycache__/",
          "report/",
          "reports/",
          "log/",
          "logs/",
          "public/",
          "build/",
        },
      },
    },
  },

  {
    -- Autocompletion
    "hrsh7th/nvim-cmp",
    event = "InsertEnter",
    dependencies = {
      {
        -- Snippet Engine & its associated nvim-cmp source
        "L3MON4D3/LuaSnip",
        build = (function()
          -- Build Step is needed for regex support in snippets.
          -- This step is not supported in many windows environments.
          if vim.fn.has("win32") == 1 or vim.fn.executable("make") == 0 then
            return
          end
          return "make install_jsregexp"
        end)(),
        dependencies = {
          {
            "rafamadriz/friendly-snippets",
            config = function()
              require("luasnip.loaders.from_vscode").lazy_load()
            end,
          },
        },
      },
      { "saadparwaiz1/cmp_luasnip" },

      -- Adds other completion capabilities.
      --  nvim-cmp does not ship with all sources by default. They are split
      --  into multiple repos for maintenance purposes.
      { "saadparwaiz1/cmp_luasnip" },
      { "hrsh7th/cmp-nvim-lua" },
      { "hrsh7th/cmp-nvim-lsp" },
      { "hrsh7th/cmp-buffer" },
      { "hrsh7th/cmp-path" },
      { "onsails/lspkind.nvim" },

      {
        "Exafunction/codeium.nvim",
        dependencies = {
          "nvim-lua/plenary.nvim",
        },
        opts = true,
      },
    },
    config = function()
      -- See `:help cmp`
      local cmp = require("cmp")
      local lspkind = require("lspkind")
      local luasnip = require("luasnip")
      luasnip.config.setup({ history = true, updateevents = "TextChanged,TextChangedI" })

      cmp.setup({
        snippet = {
          expand = function(args)
            luasnip.lsp_expand(args.body)
          end,
        },
        completion = { completeopt = "menu,menuone,noinsert" },

        -- For an understanding of why these mappings were
        -- chosen, you will need to read `:help ins-completion`
        mapping = cmp.mapping.preset.insert({
          -- Select the [n]ext item
          ["<Tab>"] = cmp.mapping.select_next_item(),
          -- Select the [p]revious item
          ["<S-Tab>"] = cmp.mapping.select_prev_item(),

          -- Scroll the documentation window [b]ack / [f]orward
          ["<C-b>"] = cmp.mapping.scroll_docs(-4),
          ["<C-f>"] = cmp.mapping.scroll_docs(4),

          -- Accept the completion.
          ["<CR>"] = cmp.mapping.confirm({ select = true }),

          -- Manually trigger a completion from nvim-cmp.
          ["<C-Space>"] = cmp.mapping.complete({}),

          -- Think of <c-l> as moving to the right of your snippet expansion.
          --  So if you have a snippet that's like:
          --  function $name($args)
          --    $body
          --  end
          --
          -- <c-l> will move you to the right of each of the expansion locations.
          -- <c-h> is similar, except moving you backwards.
          ["<C-l>"] = cmp.mapping(function()
            if luasnip.expand_or_locally_jumpable() then
              luasnip.expand_or_jump()
            end
          end, { "i", "s" }),
          ["<C-h>"] = cmp.mapping(function()
            if luasnip.locally_jumpable(-1) then
              luasnip.jump(-1)
            end
          end, { "i", "s" }),
        }),

        sources = {
          { name = "nvim_lsp" },
          { name = "nvim_lua" },
          { name = "buffer" },
          { name = "luasnip" },
          { name = "codeium" },
          { name = "path" },
        },

        formatting = {
          format = lspkind.cmp_format({
            --mode = "symbol", -- show only symbol annotations
            maxwidth = {
              -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
              -- can also be a function to dynamically calculate max width such as
              menu = 50, -- leading text (labelDetails)
              abbr = 50, -- actual suggestion item
            },
            ellipsis_char = "...", -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
            show_labelDetails = true, -- show labelDetails in menu. Disabled by default
          }),
        },
      })
    end,
  },

  {
    -- Fuzzy find
    "nvim-telescope/telescope.nvim",
    event = "VeryLazy",
    branch = "0.1.x",
    dependencies = {
      { "nvim-lua/plenary.nvim" },
      {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
        cond = function()
          return vim.fn.executable("make") == 1
        end,
      },
      { "nvim-telescope/telescope-ui-select.nvim" },
      { "nvim-tree/nvim-web-devicons", enabled = vim.g.have_nerd_font },
    },
    config = function()
      require("telescope").setup({
        defaults = {
          prompt_prefix = "   ",
          selection_caret = " ",
          entry_prefix = " ",
          sorting_strategy = "ascending",
          layout_config = {
            horizontal = {
              prompt_position = "top",
              preview_width = 0.55,
            },
            width = 0.87,
            height = 0.80,
          },
          mappings = {
            n = { ["q"] = require("telescope.actions").close },
          },
        },
      })

      pcall(require("telescope").load_extension, "fzf")
      pcall(require("telescope").load_extension, "ui-select")

      local map = vim.keymap.set
      local builtin = require("telescope.builtin")

      map("n", "<leader>gl", builtin.git_commits, { desc = "Telescope [G]it commit [L]ogs" })
      map("n", "<leader>gs", builtin.git_status, { desc = "telescope [G]it [S]tatus" })

      map("n", "<leader>fw", builtin.live_grep, { desc = "[F]ind by [W]ord" })
      map("n", "<leader>fb", builtin.buffers, { desc = "[F]ind in existing buffers" })
      map("n", "<leader>fh", builtin.help_tags, { desc = "[F]ind [H]elp" })
      map("n", "<leader>fk", builtin.keymaps, { desc = "[F]ind [K]eymaps" })
      map("n", "<leader>ff", builtin.find_files, { desc = "[F]ind [F]iles" })
      map("n", "<leader>fs", builtin.builtin, { desc = "[F]ind [S]elect Telescope" })
      map("n", "<leader>fd", builtin.diagnostics, { desc = "[F]ind [D]iagnostics" })
      map("n", "<leader>fr", builtin.resume, { desc = "[F]ind [R]esume" })
      map("n", "<leader>f.", builtin.oldfiles, { desc = '[F]ind Recent Files "." for repeat)' })

      map("n", "<leader>fz", function()
        builtin.current_buffer_fuzzy_find(require("telescope.themes").get_dropdown({
          winblend = 10,
          previewer = false,
        }))
      end, { desc = "[F]u[Z]zily search in current buffer" })

      map("n", "<leader>fo", function()
        builtin.live_grep({
          grep_open_files = true,
          prompt_title = "Live Grep in Open Files",
        })
      end, { desc = "[F]ind in [O]pen Files" })

      map("n", "<leader>fn", function()
        builtin.find_files({ cwd = vim.fn.stdpath("config") })
      end, { desc = "[F]ind [N]eovim files" })

      map("n", "<leader>fa", function()
        builtin.find_files({ follow = true, no_ignore = true, hidden = true })
      end, { desc = "[F]ind [A]ll files" })
    end,
  },
}
