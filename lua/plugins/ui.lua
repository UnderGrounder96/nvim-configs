return {
  {
    -- [[ Colorscheme ]] --
    "Mofiqul/vscode.nvim",
    lazy = false,
    priority = 1000,
    config = function()
      require("vscode").setup({
        -- Alternatively set style in setup
        style = "dark",

        -- Enable transparent background
        transparent = true,

        -- Enable italic comment
        italic_comments = true,

        -- Underline `@markup.link.*` variants
        underline_links = true,
      })
      vim.cmd.colorscheme("vscode")
    end,
  },

  {
    "nvim-lualine/lualine.nvim",
    event = "VeryLazy",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
      -- Display LSP name
      local clients_lsp = function()
        local lsp_clnts = vim.lsp.get_clients({ bufnr = 0 })

        if #lsp_clnts == 0 then
          return ""
        end

        local clients = {}
        for _, client in pairs(lsp_clnts) do
          table.insert(clients, client.name)
        end

        return table.concat(clients, "|")
      end

      require("lualine").setup({
        options = {
          -- Some useful glyphs:
          -- https://nerdfonts.com/cheat-sheet
          --        
          section_separators = { left = "", right = "" },
          component_separators = { left = "", right = "" },
          globalstatus = true,
        },
        sections = {
          lualine_b = { { "branch", color = { fg = "white" } } },
          lualine_c = { "diff" },
          lualine_x = { clients_lsp, { "filetype", icon_only = true, colored = false } },
          lualine_y = {},
          lualine_z = { "progress" },
        },
        tabline = {
          lualine_c = {
            {
              "filename",
              symbols = {
                modified = "●", -- Text to show when the buffer is modified
                readonly = "", -- Text to show when the file is non-modifiable or readonly.
              },
            },

            "diagnostics",
          },
          lualine_z = {
            {
              "tabs",
              symbols = {
                modified = ".", -- Text to show when the file is modified.
              },
            },
          },
        },
      })
    end,
  },

  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    opts = {
      preset = "modern",
    },
  },

  {
    "norcalli/nvim-colorizer.lua",
    event = "VeryLazy",
    config = function()
      require("colorizer").setup()
    end,
  },

  {
    --[[
    -- TODO|INFO|NOTE: hello there
    -- WARN|HACK: might fail
    -- BUG|FIXME: patch me quick
    -- TEST|PERF: needs testing
    --]]
    "folke/todo-comments.nvim",
    event = "VeryLazy",
    dependencies = { "nvim-lua/plenary.nvim" },
    opts = { signs = false },
  },

  {
    "lukas-reineke/indent-blankline.nvim",
    main = "ibl",
    event = "VeryLazy",
    config = function()
      local highlight = {
        "RainbowRed",
        "RainbowYellow",
        "RainbowGreen",
        "RainbowCyan",
        "RainbowBlue",
        "RainbowViolet",
        "RainbowOrange",
      }

      --[[
        create the highlight groups in the highlight setup hook, so they are reset
        every time the colorscheme changes
      --]]
      local hooks = require("ibl.hooks")
      hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
        vim.api.nvim_set_hl(0, highlight[1], { fg = "#61AFEF" })
        vim.api.nvim_set_hl(0, highlight[2], { fg = "#98C379" })
        vim.api.nvim_set_hl(0, highlight[3], { fg = "#D19A66" })
        vim.api.nvim_set_hl(0, highlight[4], { fg = "#E5C07B" })
        vim.api.nvim_set_hl(0, highlight[5], { fg = "#56B6C2" })
        vim.api.nvim_set_hl(0, highlight[6], { fg = "#C678DD" })
        vim.api.nvim_set_hl(0, highlight[7], { fg = "#E06C75" })
      end)

      require("ibl").setup({
        scope = {
          enabled = false,
        },
        indent = {
          highlight = highlight,
        },
      })
    end,
  },

  {
    -- Highlight, edit, and navigate code
    "nvim-treesitter/nvim-treesitter",
    event = "VeryLazy",
    build = ":TSUpdate",
    -- Sets main module to use for opts
    main = "nvim-treesitter.configs",
    opts = {
      ensure_installed = {
        -- web dev
        "css",
        "scss",
        "html",
        "json",
        "javascript",
        "typescript",
        "tsx",

        -- unix
        "bash",
        "make",

        -- ops
        "dockerfile",
        "terraform",
        "yaml",

        -- git
        "git_config",

        --golang
        "go",
        "gomod",

        -- script
        "groovy",
        "python",

        -- server
        "sql",
      },

      indent = {
        enable = true,
        -- disable = {
        --   "python"
        -- },
      },

      highlight = {
        enable = true,
        use_languagetree = true,
        -- Some languages depend on vim's regex highlighting system (such as Ruby) for indent rules.
        --  If you are experiencing weird indenting issues, add the language to
        --  the list of additional_vim_regex_highlighting and disabled languages for indent.
        additional_vim_regex_highlighting = { "ruby" },
      },
    },
  },
}
