return {
  {
    "stevearc/conform.nvim",
    event = "BufWritePre",
    cmd = { "ConformInfo" },
    opts = {
      notify_on_error = false,

      format_on_save = {
        lsp_format = "fallback",
      },

      formatters_by_ft = {
        lua = { "stylua" },
        go = { "gofmt" },
        javascript = { "deno_fmt" },
        javascriptreact = { "deno_fmt" },
        typescript = { "deno_fmt" },
        typescriptreact = { "deno_fmt" },
        sh = { "shfmt" },
        terraform = { "tofu_fmt" },
        python = { "autopep8" },
        ["*"] = { "trim_whitespace", "trim_newlines" },
        ["_"] = { "prettier" },
      },
    },
  },
}
