local map = vim.keymap.set
local opts = { nowait = true, noremap = true }

opts.desc = "Terminal escape"
map("t", "<esc><esc>", "<C-\\><C-N>", opts)

opts.desc = "Dont copy replaced text"
map("x", "p", '"_dP', opts)

-- [[ VISUAL ]] --
opts.desc = "Move +highlighted lines"
map("v", "J", ":m '>+1<CR>gv=gv", opts)

opts.desc = "Move -highlighted lines"
map("v", "K", ":m '<-2<CR>gv=gv", opts)

opts.desc = "Remain in visual mode, when indenting"
map("v", "<", "<gv", opts)
map("v", ">", ">gv", opts)
map("v", "=", "=gv", opts)

opts.desc = "Toggle comment (visual)"
map("v", "<leader>/", "gc", { desc = opts.desc, remap = true })

-- [[ NORMAL ]] --
opts.desc = "Toggle comment (normal)"
map("n", "<leader>/", "gcc", { desc = opts.desc, remap = true })

opts.desc = "Enter command mode"
map("n", ";", ":", opts)

opts.desc = "Clear highlights"
map("n", "<Esc>", "<cmd>nohlsearch<CR>", opts)

-- window
opts.desc = "Switch window left"
map("n", "<C-h>", "<C-w>h", opts)

opts.desc = "Switch window right"
map("n", "<C-l>", "<C-w>l", opts)

opts.desc = "Switch window down"
map("n", "<C-j>", "<C-w>j", opts)

opts.desc = "Switch window up"
map("n", "<C-k>", "<C-w>k", opts)

-- tabulator
opts.desc = "Go to next tab"
map("n", "<tab>", "<cmd>tabn<CR>", opts)

opts.desc = "Go to previous tab"
map("n", "<S-tab>", "<cmd>tabp<CR>", opts)

opts.desc = "Tab new"
map("n", "<C-n>", "<cmd>tabnew<CR>:e ", opts)

-- file/buffer
opts.desc = "File close"
map("n", "<C-w>", "<cmd>bdelete!<CR>", opts)

opts.desc = "File save"
map("n", "<C-s>", "<cmd>update<CR>", opts)

opts.desc = "File copy"
map("n", "<C-c>", "<cmd>%y<CR>", opts)

-- terminal
opts.desc = "Open terminal horizontally"
map("n", "<leader>th", function()
  vim.cmd.new()
  vim.cmd.term()
end, opts)

opts.desc = "Open terminal vertically"
map("n", "<leader>tv", function()
  vim.cmd.vnew()
  vim.cmd.term()
end, opts)
