local fn = vim.fn
local api = vim.api

local autocmd = api.nvim_create_autocmd
local augroup = api.nvim_create_augroup("user_cmds", { clear = true })

local patterns = "*.*,*file*"

-- auto resize panes when resizing nvim window
autocmd("VimResized", {
  group = augroup,
  command = "tabdo wincmd =",
})

-- auto-save
autocmd({ "CursorHold", "FocusLost" }, {
  group = augroup,
  pattern = patterns,
  desc = "auto save on any text change",
  callback = function()
    if not vim.bo.modifiable or not vim.bo.modified then
      return
    end

    vim.schedule(function()
      vim.cmd("silent update ++p")
      vim.notify_once("💾: " .. fn.expand("%") .. " @ " .. fn.strftime("%T"))
    end)
  end,
})

-- terminal
autocmd("TermOpen", {
  group = augroup,
  desc = "open terminal in insert mode",
  callback = function()
    vim.opt.number = false
    vim.opt.relativenumber = false
    vim.cmd.startinsert()
  end,
})

autocmd("TermClose", {
  group = augroup,
  desc = "insert new line, after closing terminal",
  command = "call feedkeys('\\<CR>')",
})

autocmd("BufEnter", {
  group = augroup,
  pattern = "term://*",
  desc = "auto enter when terminal mode",
  command = "startinsert",
})

-- folds
autocmd("BufUnload", {
  group = augroup,
  pattern = patterns,
  desc = "save view (folds), when closing file",
  command = "mkview",
})

autocmd("FileReadPre", {
  group = augroup,
  pattern = patterns,
  desc = "load view (folds), when opening file",
  command = "silent! loadview",
})
