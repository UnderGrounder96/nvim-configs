local data = vim.fn.stdpath("data")

-- clone lazy
vim.schedule(function()
  local lazypath = data .. "/lazy/lazy.nvim"

  if not (vim.uv or vim.loop).fs_stat(lazypath) then
    local lazyrepo = "https://github.com/folke/lazy.nvim.git"

    vim.notify_once("Attempting to clone lazy.nvim")

    local lazyerr = vim.fn.system({
      "git",
      "clone",
      "--filter=blob:none",
      "--branch=stable",
      lazyrepo,
      lazypath,
    })

    if vim.v.shell_error ~= 0 then
      error("Error cloning lazy.nvim:\n" .. lazyerr)
    end
  end

  vim.opt.rtp:prepend(lazypath)
end)

-- bootstrap lazy
vim.schedule(function()
  local lazyconfig = {
    lockfile = data .. "/lazy-lock.json",

    defaults = { lazy = true, vesion = "*" },

    spec = { import = "plugins" },

    change_detection = {
      notify = false,
    },

    ui = {
      icons = {
        ft = "",
        loaded = "",
      },
    },

    performance = {
      rtp = {
        disabled_plugins = {
          "2html_plugin",
          "tohtml",
          "getscript",
          "getscriptPlugin",
          "gzip",
          "logipat",
          "netrw",
          "netrwPlugin",
          "netrwSettings",
          "netrwFileHandlers",
          "matchit",
          "tar",
          "tarPlugin",
          "rrhelper",
          "spellfile_plugin",
          "vimball",
          "vimballPlugin",
          "zip",
          "zipPlugin",
          "tutor",
          "rplugin",
          "syntax",
          "synmenu",
          "optwin",
          "compiler",
          "bugreport",
          "ftplugin",
        },
      },
    },
  }

  require("lazy").setup(lazyconfig)
end)
