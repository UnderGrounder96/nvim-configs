-- https://neovim.io/doc/user/options.html
local opt = vim.opt
local global = vim.g

-- globals
global.mapleader = " "
global.maplocalleader = global.mapleader

global.loaded_node_provider = 0
global.loaded_perl_provider = 0
global.loaded_ruby_provider = 0
global.loaded_python3_provider = 0

-- line number
opt.number = true
opt.relativenumber = true

-- time
opt.updatetime = 1700

-- file
opt.filetype = "on"
opt.fileformat = "unix"
opt.undofile = true
opt.swapfile = false

-- text manipulation
opt.formatoptions = "l"
vim.schedule(function()
  opt.shortmess:append("sI")
  opt.whichwrap:append("<>[]hl")
  opt.clipboard:append("unnamedplus")
end)

-- search
opt.smartcase = true
opt.ignorecase = true

-- split
opt.splitbelow = true
opt.splitright = true

-- indenting
opt.tabstop = 2
opt.shiftwidth = 0
opt.softtabstop = -1
opt.expandtab = true
opt.linebreak = true
opt.autoindent = true
opt.smartindent = true
opt.breakindent = true

-- folding
opt.foldlevel = 6
opt.foldmethod = "expr"
opt.foldexpr = "nvim_treesitter#foldexpr()"

-- appearance
opt.showmode = false
opt.spell = true
opt.list = true
opt.scrolloff = 4
opt.termguicolors = true
opt.background = "dark"
opt.signcolumn = "yes"
opt.colorcolumn = "80"
opt.cursorline = true
opt.cursorlineopt = "number"
