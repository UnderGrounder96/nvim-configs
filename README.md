# nvim-configs

This repo contains my neovim configs.

# LICENSE

Licensed under the [MIT](./LICENSE) license.
