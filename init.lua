-- load configs
require("configs.options")
require("configs.mappings")
require("configs.autocmds")

-- load plugin manager
require("configs.lazy")
