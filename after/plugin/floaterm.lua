local api = vim.api

local state = {
  buf = -1,
  win = -1,
}

local create_floating_window = function(opts)
  opts = opts or {}

  opts.width = opts.width or math.floor(vim.o.columns * 0.7)
  opts.height = opts.height or math.floor(vim.o.lines * 0.7)

  -- Create new buffer from scratch
  if not api.nvim_buf_is_valid(opts.buf) then
    opts.buf = api.nvim_create_buf(false, true)
  end

  return {
    buf = opts.buf,

    -- Create the floating window
    win = api.nvim_open_win(opts.buf, true, {
      relative = "editor",
      width = opts.width,
      height = opts.height,

      -- Calculate the position to center the window
      col = math.floor((vim.o.columns - opts.width) / 2),
      row = math.floor((vim.o.lines - opts.height) / 2),

      border = "double",
    }),
  }
end

vim.keymap.set("n", "<leader>tf", function()
  if api.nvim_win_is_valid(state.win) then
    api.nvim_win_hide(state.win)
    return
  end

  state = create_floating_window({ buf = state.buf })
  if vim.bo[state.buf].buftype ~= "terminal" then
    vim.cmd.term()
  end
end, { desc = "Terminal toggle", noremap = true })
